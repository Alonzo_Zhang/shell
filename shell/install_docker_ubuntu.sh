#!/bin/bash
COLOR="echo -e \\033[1;31m"
END="\033[m"
DOCKER_COMPOSE_VERSION="1.25.3"

install_docker(){
${COLOR}"开始安装 Docker....."${END}
sleep 1 

apt update
apt  -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
apt -y update
apt-cache madison docker-ce
#apt -y  install docker-ce=5:19.03.5~3-0~ubuntu-bionic  docker-ce-cli=5:19.03.5~3-0~ubuntu-bionic
apt-get -y install docker-ce docker-ce-cli containerd.io

mkdir -p /etc/docker
tee /etc/docker/daemon.json <<-'EOF'
{
    "data-root": "/data/docker",
    "default-address-pools":
        [
            {
                "base": "198.18.0.0/16",
                "size": 24
            }
        ],
    "registry-mirrors":
        [
            "https://si7y70hh.mirror.aliyuncs.com",
            "http://hub-mirror.c.163.com",
            "http://docker.mirrors.ustc.edu.cn",
            "https://registry.docker-cn.com"
        ]
}
EOF
systemctl daemon-reload
systemctl enable docker
systemctl restart docker
docker version && ${COLOR}"Docker 安装完成"${END} ||  ${COLOR}"Docker 安装失败"${END}
}

install_docker_compose(){
${COLOR}"开始安装 Docker compose....."${END}
sleep 1

curl -L https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

docker-compose --version &&  ${COLOR}"Docker Compose 安装完成"${END} ||  ${COLOR}"Docker compose 安装失败"${END}
}

pull_images(){
images=("ubuntu" "mysql" "centos" "redis" "nginx" "ubuntu:18.04")
for image in ${images[@]}
do
	docker pull $image
done
}

dpkg -s docker-ce &> /dev/null && ${COLOR}"Docker已安装"${END} || install_docker

docker-compose --version &> /dev/null && ${COLOR}"Docker Compose已安装"${END} || install_docker_compose

#pull_gitlab_images
