#!/bin/bash
date=$(date "+%Y-%m-%d %H:%M:%S")
i=1
list=()
for file in $(find /root/test1  -type f -size +10k)
do
    list[$i]=\"$file\"
    i=$(expr $i + 1)
done
find /root/test1  -type f -size +10k -exec truncate -s 0 {} \;
if [ "$?" == 0 ]
then
    echo "{clear_file_time:$date,clear_file_name:[${list[@]}],status:success}">>/var/log/clear_large_files.log
else
    echo "{clear_file_time:$date,clear_file_name:[${list[@]}],status:error}">>/var/log/clear_large_files.log
fi
